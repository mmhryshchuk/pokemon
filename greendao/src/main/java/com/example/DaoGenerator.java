package com.example;

import java.util.Properties;

import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;

public class DaoGenerator {




    private int appVersionCode = 1;

    Schema schema;

    public DaoGenerator(){
        Properties properties = new Properties();
    }

    public static void main(String[] args) throws Exception {
        new DaoGenerator().generate();
    }

    public void generate() throws Exception{
        schema = new Schema(appVersionCode,"com.mmh.pokemontask.dto.daogen");

        generateProfessor();

        new de.greenrobot.daogenerator.DaoGenerator().generateAll(schema,"app/src/main/java");


    }

    public Entity generateProfessor(){
        Entity latlng = schema.addEntity("Latlang");
        latlng.addDoubleProperty("lat");
        latlng.addDoubleProperty("lng");
        latlng.addIntProperty("type");
        return latlng;
    }

}
