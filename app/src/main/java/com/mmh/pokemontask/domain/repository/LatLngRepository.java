package com.mmh.pokemontask.domain.repository;

import com.mmh.pokemontask.dto.daogen.DaoSession;
import com.mmh.pokemontask.dto.daogen.Latlang;
import com.mmh.pokemontask.dto.daogen.LatlangDao;

import java.util.List;

import de.greenrobot.dao.query.QueryBuilder;

/**
 * Created by mmhryshchuk on 13.08.16.
 */
public class LatLngRepository {

    LatlangDao latlangDao;
    DaoSession daoSession;

    public LatLngRepository(DaoSession daoSession) {
        this.latlangDao = daoSession.getLatlangDao();
        this.daoSession = daoSession;
    }

    public void addLatLang(final Latlang latlang){
        daoSession.runInTx(new Runnable() {
            @Override
            public void run() {
                latlangDao.insertOrReplace(latlang);
            }
        });
    }

    public List<Latlang> getAllLatLang(){
        return latlangDao.loadAll();
    }

    public List<Latlang> getLatLangsForType(int type){
        QueryBuilder<Latlang> qb = latlangDao.queryBuilder();
        return qb.where(
                LatlangDao.Properties.Type.in(type)
        ).list();
    }
}
