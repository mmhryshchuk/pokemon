package com.mmh.pokemontask.domain.mappers;

import com.google.android.gms.maps.model.LatLng;
import com.mmh.pokemontask.dto.daogen.Latlang;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mmhryshchuk on 13.08.16.
 */
public class LangWrapper {

    public static List<LatLng> wrap(List<Latlang> latlangs){
        List<LatLng> list = new ArrayList<>();
        for (Latlang latlang : latlangs){
            LatLng latLng = new LatLng(latlang.getLat(),latlang.getLng());
            list.add(latLng);
        }
        return list;
    }
}
