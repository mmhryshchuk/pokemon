package com.mmh.pokemontask.domain.controller;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.mmh.pokemontask.domain.repository.LatLngRepository;
import com.mmh.pokemontask.dto.daogen.DaoMaster;
import com.mmh.pokemontask.dto.daogen.Latlang;

import java.util.List;

/**
 * Created by mmhryshchuk on 13.08.16.
 */
public class LatLangController {

    LatLngRepository repository;

    public LatLangController(Context context){
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(context,"Latlng-db",null);
        SQLiteDatabase db = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        repository = new LatLngRepository(daoMaster.newSession());
    }

    public void addLatLang(Latlang latlang){
        repository.addLatLang(latlang);
    }

    public List<Latlang> getAll(){
        return repository.getAllLatLang();
    }

    public List<Latlang> getForType(int type){
        return repository.getLatLangsForType(type);
    }

}
