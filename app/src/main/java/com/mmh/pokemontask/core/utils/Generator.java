package com.mmh.pokemontask.core.utils;

import android.content.Context;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mmh.pokemontask.R;
import com.mmh.pokemontask.domain.controller.LatLangController;
import com.mmh.pokemontask.dto.daogen.Latlang;
import com.mmh.pokemontask.ui.start.MainActivity;

import java.util.Random;

/**
 * Created by mmhryshchuk on 13.08.16.
 */
public class Generator {
    Context context;

    public Generator( Context context) {
        this.context = context;
    }
    LatLangController controller;
    public void generate(LatLng lng) {
        controller = new LatLangController(context);
        Random random = new Random();
        double radius = 20000;
        for (int i = 0; i < 200; i++) {
            double radiusInDegrees = radius / 111000f;
            double latitude = lng.latitude;
            double longitude = lng.longitude;
            double u = random.nextDouble();
            double v = random.nextDouble();
            double w = radiusInDegrees * Math.sqrt(u);
            double t = 2 * Math.PI * v;
            double x = w * Math.cos(t);
            double y = w * Math.sin(t);

            double new_x = x / Math.cos(longitude);

            double foundLongitude = new_x + longitude;
            double foundLatitude = y + latitude;
            LatLng newLng = new LatLng(foundLatitude,foundLongitude);

            controller.addLatLang(new Latlang(foundLatitude,foundLongitude,2));
        }
    }
}
