package com.mmh.pokemontask.ui.start;

/**
 * Created by mmhryshchuk on 12.08.16.
 */
public interface MainView {
    void openMapScreen(int type);
}
