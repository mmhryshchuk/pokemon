package com.mmh.pokemontask.ui.map;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by mmhryshchuk on 12.08.16.
 */
public interface MapView {
    void showCurrentLocation(GoogleMap googleMap, LatLng latLng);
    void showCircleRadius(GoogleMap googleMap,LatLng centerPosition, int radius);
    void showAnimals(int type);
    void hideAnimals(int type);
    void hideCurrentAnimal();


}
