package com.mmh.pokemontask.ui.start;

import com.mmh.pokemontask.core.mvp.BasePresenter;

/**
 * Created by mmhryshchuk on 12.08.16.
 */
public class MainPresenterImpl extends BasePresenter<MainView> implements MainPresenter {

    public MainPresenterImpl() {
    }

    @Override
    public void onPokemonClick(int type) {
        if (getView() == null) return;
        getView().openMapScreen(type);
    }

    @Override
    public void onZombieClick(int type) {
        if (getView() == null) return;
        getView().openMapScreen(type);
    }
}
