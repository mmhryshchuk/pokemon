package com.mmh.pokemontask.ui.start;

import com.mmh.pokemontask.core.mvp.Presenter;

/**
 * Created by mmhryshchuk on 12.08.16.
 */
public interface MainPresenter extends Presenter<MainView> {

    void onPokemonClick(int type);
    void onZombieClick(int type);
}
