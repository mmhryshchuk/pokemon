package com.mmh.pokemontask.ui.map;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.mmh.pokemontask.core.mvp.Presenter;

/**
 * Created by mmhryshchuk on 12.08.16.
 */
public interface MapPresenter extends Presenter<MapView> {

    void loadAnimals(int type);
    LatLng getCurrentLocation();
    void displayCLocation();
}
