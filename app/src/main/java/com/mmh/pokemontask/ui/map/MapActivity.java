package com.mmh.pokemontask.ui.map;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mmh.pokemontask.R;
import com.mmh.pokemontask.core.utils.CircleDrawUtils;
import com.mmh.pokemontask.core.utils.Generator;
import com.mmh.pokemontask.domain.controller.LatLangController;
import com.mmh.pokemontask.domain.mappers.LangWrapper;
import com.mmh.pokemontask.ui.LocationProvider;
import com.mmh.pokemontask.ui.start.MainActivity;

import java.util.List;

public class MapActivity extends FragmentActivity implements LocationProvider.LocationCallback , OnMapReadyCallback {

    // LogCat tag
    private static final String TAG = MainActivity.class.getSimpleName();

    private static final String TYPE = "TYPE";
    private static final int RADIUS = 1000;

    private GoogleMap mMap;
    private CircleDrawUtils circleDrawUtils;
    private LatLng myPosition;
    LatLangController controller;


    public static void start(Activity activity, int type) {
        Intent intent = new Intent(activity, MapActivity.class);
        intent.putExtra(TYPE, type);
        activity.startActivity(intent);
    }

    private LocationProvider mLocationProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        setUpMapIfNeeded();
        mLocationProvider = new LocationProvider(this, this);
        controller = new LatLangController(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
        mLocationProvider.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mLocationProvider.disconnect();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        setUpMap();

    }

    private void setUpMapIfNeeded() {
        if (mMap == null) {
            SupportMapFragment mapFragment = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.google_map));
            mapFragment.getMapAsync(this);
            if (mMap != null) {
                setUpMap();
            }
        }
    }


    private void setUpMap() {

    }

    public void handleNewLocation(Location location) {
        Log.d(TAG, location.toString());

        double currentLatitude = location.getLatitude();
        double currentLongitude = location.getLongitude();
        myPosition= new LatLng(currentLatitude, currentLongitude);
        circleDrawUtils = new CircleDrawUtils(mMap);
        circleDrawUtils.drawCircle(myPosition,RADIUS);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myPosition,
                15.0f));
        mMap.addMarker(new MarkerOptions().position(new LatLng(currentLatitude, currentLongitude)).title("Current Location"));
        MarkerOptions options = new MarkerOptions()
                .position(myPosition)
                .title("I am here!");
        mMap.addMarker(options);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myPosition,15.0f));
        if (controller.getAll().size() == 0) {
            Generator generator = new Generator(this);
            generator.generate(myPosition);
            showType(getIntent().getIntExtra(TYPE, 2),mMap,myPosition);
        } else {
            showType(getIntent().getIntExtra(TYPE, 2),mMap,myPosition);
        }

    }

    public void showType(int type, GoogleMap googleMap,LatLng myPosition){
        BitmapDescriptor icon = null;
        if (type == MainActivity.TYPE_POKEMON){
            icon= BitmapDescriptorFactory.fromResource(R.drawable.ic_pikachu);
        } else if (type == MainActivity.TYPE_ZOMBIE){
            icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_zombie_2);

        }
        List<LatLng> list = LangWrapper.wrap(controller.getAll());
        Log.d("SIZEPOKEMON", String.valueOf(list.size()));
        showAllowed(list,myPosition,googleMap,icon);
        hideNearMy(list,myPosition);
    }

    public void showAllowed(List<LatLng> list,LatLng myPosition,GoogleMap googleMap,BitmapDescriptor icon){
        for (LatLng lng : list){
            double latit = lng.latitude;
            double longit = lng.longitude;
          final float[] distance = new float[2];
          Location.distanceBetween(myPosition.latitude,myPosition.longitude,latit,longit,distance);
            if (distance[0] < RADIUS){
                MarkerOptions marker = new MarkerOptions().position(lng).title("Pokemon"  ).icon(icon);
                googleMap.addMarker(marker);
            }
        }
    }

    public void hideNearMy(List<LatLng> list,LatLng myPosition){
        for (LatLng lng : list){
            double latit = lng.latitude;
            double longit = lng.longitude;
            final float[] distance = new float[2];
            Location.distanceBetween(myPosition.latitude,myPosition.longitude,latit,longit,distance);
            if (distance[0] < 50){
                list.remove(lng);
            }
        }
    }

}