package com.mmh.pokemontask.ui.map;

import android.app.Activity;

import com.google.android.gms.maps.model.LatLng;
import com.mmh.pokemontask.core.mvp.BasePresenter;

/**
 * Created by mmhryshchuk on 12.08.16.
 */
public class MapPresenterImpl extends BasePresenter<MapView> implements MapPresenter {

    Activity activity;

    public MapPresenterImpl(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void loadAnimals(int type) {

    }

    @Override
    public LatLng getCurrentLocation() {
        return null;
    }

    @Override
    public void displayCLocation() {

    }
}
