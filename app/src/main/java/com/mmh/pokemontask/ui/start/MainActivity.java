package com.mmh.pokemontask.ui.start;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.mmh.pokemontask.R;
import com.mmh.pokemontask.ui.map.MapActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements MainView {

    public static final int TYPE_POKEMON = 1;
    public static final int TYPE_ZOMBIE = 2;

    MainPresenter mainPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mainPresenter = new MainPresenterImpl();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mainPresenter.attachView(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mainPresenter.detachView();
    }

    @OnClick(R.id.pokemon)
    public void onPokemon(){
        mainPresenter.onPokemonClick(TYPE_POKEMON);
    }

    @OnClick(R.id.zombie)
    public void onZombie(){
        mainPresenter.onZombieClick(TYPE_ZOMBIE);
    }

    @Override
    public void openMapScreen(int type) {
        MapActivity.start(this,type);
    }
}
